This directory contains an upper case [.GIT](.GIT) directory.

This is likely the vector of the Windows remote execution: <https://github.com/blog/1938-git-client-vulnerability-announced>

It has been split from the main test repo because GitHub won't accept it. TODO do they run on Mac? GitLab accepts it.

GitHub gives:

    remote: error: object 7c44d28ec6aa2a7058a0f309fe4de38a448af241:contains '.git'
    remote: fatal: Error in object
    error: unpack failed: index-pack abnormal exit
    To git@github.com:cirosantilli/test-GIT.git
     ! [remote rejected] master -> master (unpacker error)
    error: failed to push some refs to 'git@github.com:cirosantilli/test-GIT.git'

Very interesting.
